package ictgradschool.industry.test04.q1;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Andrew Meads on 9/01/2018.
 */
public class Starfield {

    private List<Star> stars;

    public Starfield(int width, int height) {
        stars=new ArrayList<>();
        int NUMBER_OF_STARS=100;
        // TODO Step 2a.
        for(int i=0;i<NUMBER_OF_STARS;i++) {
            int randomWidth = (int) (Math.random() * width);
            int randomHeight = (int) (Math.random() * height);
            int randomSize = (int) (Math.random() * ((6 - 2) + 1) + 2);
            int randomSpeed = (int) (Math.random() * ((11 - 5) + 1) + 5);
        Star s = new Star(randomWidth,randomHeight,randomSize,randomSpeed);
        stars.add(s);
        }

    }

    public void move(int windowWidth, int windowHeight) {

        // TODO Step 2b.
        for(Star star: stars){
            star.move(windowWidth,windowHeight);
        }
    }

    public void paint(Graphics g) {

        for(Star star: stars){
            star.paint(g);
        }
        // TODO Step 2c.
    }

}
