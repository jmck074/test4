package ictgradschool.industry.test04.q1;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.io.IOException;

import static com.sun.java.accessibility.util.AWTEventMonitor.addKeyListener;

/**
 * Created by Andrew Meads on 9/01/2018.
 */
public class Spaceship {

    private static final int SPRITE_WIDTH = 100, SPRITE_HEIGHT = 100;

    private int x, y, width, height, speed;

    private Direction direction = Direction.Up;

    private Image image;

    public Spaceship(int x, int y, int speed) {

        try {
            this.image = ImageIO.read(new File("spaceship.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.x = x;
        this.y = y;
        this.width = SPRITE_WIDTH;
        this.height = SPRITE_HEIGHT;
        this.speed = speed;

    }


    public void move(Direction direction, int windowWidth, int windowHeight) {

        // TODO Step 6.
        if(direction==Direction.Up && y>0){y=y-speed;}
        if(direction==Direction.Down && y<windowHeight-height){y=y+speed;}
        if(direction==Direction.Left&&x>0){x=x-speed;}
        if(direction==Direction.Right&&x<windowWidth-width){x=x+speed;}

    }

    //Adding new method (getter) to share info required for move() call in panel
    public Direction shipdata(){
        return direction;
    }

    public void paint(Graphics g) {

        // TODO Step 7 (modify this).
//if(direction==Direction.UP){
//int x1=0;
        //int x2=width;}

        //g.drawImage(image, x, y, x + width, y + height, 0, 0, width, height, null);


if(this.direction==direction.Up){g.drawImage(image, x, y, x + width, y + height, 0, 0, width, height, null);}
        else if(this.direction==direction.Down){g.drawImage(image, x, y, x + width, y + height, width, 0, 2*width, height, null);}
        else if(this.direction==direction.Left){g.drawImage(image, x, y, x + width, y + height, 2*width, 0, 3*width, height, null);}
        else if(this.direction==direction.Right){g.drawImage(image, x, y, x + width, y + height, 3*width, 0, 4*width, height, null);}
    }


}
