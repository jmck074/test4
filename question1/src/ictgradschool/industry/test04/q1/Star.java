package ictgradschool.industry.test04.q1;

import java.awt.*;

/**
 * Created by Andrew Meads on 9/01/2018.
 */
public class Star {

    private int x, y, size, speed;

    public Star(int x, int y, int size, int speed) {
        this.x = x;
        this.y = y;
        this.size = size;
        this.speed = speed;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getSize() {
        return size;
    }

    public int getSpeed() {
        return speed;
    }

    public void move(int windowWidth, int windowHeight) {
        // TODO Step 1a.
        //move star down
        y=y+speed;
        //if y moves off screen, move to top or y=0
        if(y>windowHeight){y=0;}
    }

    public void paint(Graphics g) {
        // TODO Step 1b.
        //make star white
        //oval has x,y, width, height,
        g.setColor(Color.WHITE);
        g.fillOval(x,y,size,size);
    }
}
