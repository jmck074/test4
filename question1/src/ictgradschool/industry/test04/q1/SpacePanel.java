package ictgradschool.industry.test04.q1;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * Created by Andrew Meads on 9/01/2018.
 *
 * TODO Steps 4 and 5 (implement interfaces)
 */
public class SpacePanel extends JPanel implements ActionListener, KeyListener{

    public static final int PREFERRED_WIDTH = 500;
    public static final int PREFERRED_HEIGHT = 500;

    private Starfield stars;

    private Spaceship ship;

    private Direction moveDirection = Direction.None;

    private Timer timer;

    public SpacePanel() {
        setPreferredSize(new Dimension(PREFERRED_WIDTH, PREFERRED_HEIGHT));

        this.stars = new Starfield(PREFERRED_WIDTH, PREFERRED_HEIGHT);

        this.ship = new Spaceship(PREFERRED_WIDTH / 2, PREFERRED_HEIGHT / 2, 10);

        //add listener
        addKeyListener(this);

        // TODO Step 4b.
timer = new Timer(20,this);
    }

    // TODO Step 4c (modify the two methods below).

    public void start() {
//timer starts when start is clicked and starts generating events
        timer.start();
    }

    public void stop() {
        //timer stops when stop is clicked
timer.stop();
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        g.setColor(Color.black);
        g.fillRect(0, 0, getWidth(), getHeight());
        //paint the stars
        stars.paint(g);
        // TODO Step 3.
        ship.paint(g);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
//timer generates events causing the stars to move. repaint() redraws the screen each time the timer ticks.
        if(e.getSource()==timer){
    stars.move(getWidth(),getHeight());
    ship.move(moveDirection,getWidth(),getHeight() );
    requestFocusInWindow();
    repaint();
}
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
//change direction based on key press
        if (e.getKeyCode() == KeyEvent.VK_UP) {
            moveDirection = Direction.Up;
        }
        if (e.getKeyCode() == KeyEvent.VK_DOWN) {
            moveDirection = Direction.Down;
        }
        if (e.getKeyCode() == KeyEvent.VK_LEFT) {
            moveDirection = Direction.Left;
        }
        if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
            moveDirection = Direction.Right;
        }
        else{moveDirection=Direction.None;}
    }

    @Override
    public void keyReleased(KeyEvent e) {
//currently four methods just copy and pasted, will put into one if time
//also wasn't clear if direction should be mapped to key direction on release OR if direction should have been mapped to None
        if (e.getKeyCode() == KeyEvent.VK_UP) {
            moveDirection = Direction.Up;
        }
        else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
            moveDirection = Direction.Down;
        }
        else if (e.getKeyCode() == KeyEvent.VK_LEFT) {
            moveDirection = Direction.Left;
        }
        else if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
            moveDirection = Direction.Right;
        }
    }
}