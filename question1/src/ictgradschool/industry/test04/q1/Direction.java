package ictgradschool.industry.test04.q1;

/**
 * Created by Andrew Meads on 9/01/2018.
 */
public enum Direction {

    Up, Down, Left, Right, None
}
